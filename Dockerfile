FROM ubuntu:18.04

ENV DEBIAN_FRONTEND=noninteractive
RUN apt-get update \
  && apt-get install -y gnupg make ruby asciidoc xvfb ffmpeg \
  && echo 'deb http://downloads.skewed.de/apt/bionic bionic universe' > /etc/apt/sources.list.d/graph-tool.list \
  && apt-key adv --keyserver pgp.skewed.de --recv-key 612DEFB798507F25 \
  && apt-get update \
  && apt-get install -y python3-graph-tool
ADD xvfb.sh /xvfb.sh

ENTRYPOINT ["/xvfb.sh"]
