#!/bin/bash

# start xvfb
Xvfb :1 -screen 0 1024x768x16 &
sleep 1
export DISPLAY=:1.0

exec "$@"
